﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour {

    public string ForwardInput;
    public string BackwardInput;
    public string LeftInput;
    public string RightInput;
    public float Speed;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(ForwardInput))
        {
            transform.Translate(0, 0, Speed * Time.deltaTime);
        }
        if (Input.GetKey(BackwardInput))
        {
            transform.Translate(0, 0, -Speed * Time.deltaTime);
        }
        if (Input.GetKey(RightInput))
        {
            transform.Translate(Speed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(LeftInput))
        {
            transform.Translate(-Speed * Time.deltaTime, 0, 0);
        }
    }
}
